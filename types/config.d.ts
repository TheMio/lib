export type PathType =
	| 'Backgrounds'
	| 'Import'
	| 'Temp'
	| 'Logs'
	| 'SessionExports'
	| 'Previews'
	| 'Avatars'
	| 'Banners'
	| 'StreamFiles'
	| 'BundledBackgrounds'
	| 'DB'
	| 'Bin';
